/**
 * @file main.c
 */
#include <stdio.h>

#include "grille.h"
#include "io.h"
#include "jeu.h"

/**
 * \brief Main function
 * @param argc
 * @param argv
 * @return
 */
int main (int argc, char ** argv) {

	if (argc != 2 )
	{
		printf(
				"\x1B[1;31m usage :\x1B[33m main \x1B[1m<\x1B[4;34mfichier grille\x1B[24;33m>\x1B[0m");

		return errno;
	}

	grille g, gc;
	init_grille_from_file(argv[1],&g);
	alloue_grille (g.nbl, g.nbc, &gc);

	debut_jeu(&g, &gc);
	libere_grille(&g);
	libere_grille(&gc);

	return 0;
}
