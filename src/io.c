#include "io.h"
#include <ctype.h>


int cyclique = 1;
uint evolution = 0;
uint vieillisement = 0;

void affiche_trait (int c){
	int i;
	for (i=0; i<c; ++i) printf ("|---");
	printf("|\n");
	return;
}

void affiche_ligne (int c, int* ligne){
	int i;
	for (i=0; i<c; ++i)
		if (ligne[i] == 0 ) printf ("|   ");
		else if(ligne[i] == -1) printf ("| X ");
		else printf ("| %d ",ligne[i]);

	printf("|\n");


	return;
}

void affiche_grille (grille g){
	int i, l=g.nbl, c=g.nbc;
	printf("\n");
	affiche_trait(c);
	for (i=0; i<l; ++i) {
		affiche_ligne(c, g.cellules[i]);
		affiche_trait(c);
	}
	printf("\n");
	return;
}

void efface_grille (grille g){
	printf("\n\x1B[%dA",g.nbl*2 + 5);
//	printf("\n\x1B[%dA\x1B[0m",g.nbl*2 + 5);
}

void debut_jeu(grille *g, grille *gc){

	printf("\n\n");
	affiche_grille(*g);
	char input = getchar();
	compte_voisins_vivants = compte_voisins_vivants_c;

	while (input != 'q') { // touche 'q' pour quitter

		switch (input) {
			case '\n' :{ // touche "entree" pour évoluer
				efface_grille(*g);
				printf("\x1B[3A");
				if (cyclique) printf("Calc cyclyque\n");
				else printf("Calc non-cyclique\n");
				if (vieillisement) printf("Vielliesement On\n");
				else printf("Vielliesement Off\n");
				printf("Evolution: %4d\n",evolution);

				evolue(g,gc);
				evolution++;
				affiche_grille(*g);
				break;
			}

			case 'c':{
				cyclique = !cyclique;
				if(cyclique){
					compte_voisins_vivants = &compte_voisins_vivants_c;
				}else{
					compte_voisins_vivants = &compte_voisins_vivants_nc;
				}
				break;
			}



			case 'v':
				if (vieillisement == 0) vieillisement = 1;
				else vieillisement = 0;
				break;

			case 'n':{
				evolution = 0; // drop evolution on new grille load
				char buffer[128],confirm = 'n';

				printf("\n\x1B[1A");
				printf("\nInseres une nouvelle grille:\n:> ");
				//todo error handler for scanf
				if(!scanf("%s",buffer))
					perror("Scanf error buffer");

				printf("\n\x1B[1A");
				printf("La nouvelle grille a charger: %s [Y/N]\n",buffer);
				if (!scanf("\n%c",&confirm))
					perror("Scanf error confirm");

				//if confirm
				if ( ((char) tolower(confirm)) == 'y') {
					if (buffer[0] == '\n'){
						printf("\n\t\x1B[1;31mInserer un nom du fichier\x1B[0m\t\n");
					}
					else{
						evolution = 0;
//						efface_grille(*g);

						libere_grille(g);
						libere_grille(gc);

						init_grille_from_file(buffer,g);
						alloue_grille(g->nbl,g->nbc,gc);
						//todo error handler for system

						if(system("clear & clear"))
							perror("Error system clear");
						getchar();

						printf("\x1B[2A");
						if (cyclique)
							printf("Calc cyclyque\n");
						else
							printf("Calc non-cyclique\n");
						printf("Evolution: %4d\n",evolution);

						affiche_grille(*g);
					}

				}
				break;
			}
			default :
			{ // touche non traitée
				printf("\n\x1B[1A");
				break;
			}
		}
		input = getchar();
	}
}

void afficje_evolution(uint evolution) {
	printf("\x1B[1;95m~~~ Evolution : %3u ~~~\x1B[0m\n",evolution);
}
