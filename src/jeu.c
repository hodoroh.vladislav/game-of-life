#include "jeu.h"

int compte_voisins_vivants_c (int i, int j, grille g){
	int v = 0, l=g.nbl, c = g.nbc;
	v+= est_vivante(modulo(i-1,l),modulo(j-1,c),g);
	v+= est_vivante(modulo(i-1,l),modulo(j,c),g);
	v+= est_vivante(modulo(i-1,l),modulo(j+1,c),g);
	v+= est_vivante(modulo(i,l),modulo(j-1,c),g);
	v+= est_vivante(modulo(i,l),modulo(j+1,c),g);
	v+= est_vivante(modulo(i+1,l),modulo(j-1,c),g);
	v+= est_vivante(modulo(i+1,l),modulo(j,c),g);
	v+= est_vivante(modulo(i+1,l),modulo(j+1,c),g);

	return v;
}


int compte_voisins_vivants_nc(int i, int j, grille g){
	int lines = g.nbl, cols = g.nbc, v = 0;
	//top
	if(i >= 1)
		v += est_vivante(i-1,j,g);

	//top-left
	if((i >= 1) && (j >= 1))
		v += est_vivante(i-1,j-1,g);

	//left
	if(j >= 1)
		v += est_vivante(i,j-1,g);

	//botom-left
	if ((i < lines-1) && (j >= 1))
		v += est_vivante(i+1,j-1,g);

	//bottom
	if(i < lines-1)
		v += est_vivante(i+1,j,g);

	//bottom-right
	if ((i < lines-1) && (j < cols-1))
		v += est_vivante(i+1,j+1,g);

	//right
	if(j < cols-1)
		v += est_vivante(i, j+1,g);

	//top-right
	if (j < cols-1 && i >= 1)
		v += est_vivante(i-1,j+1,g);

	return v;
}

void evolue (grille *g, grille *gc){
	copie_grille (*g,*gc); // copie temporaire de la grille
	int i,j,l=g->nbl, c = g->nbc,v;
	extern uint vieillisement;

	for (i=0; i<l; i++)
	{
		for (j=0; j<c; ++j)
		{
			if (est_viable(i,j,*gc)) {
				v = (*compte_voisins_vivants)(i, j, *gc);
				if (est_vivante(i,j,*g))
				{ // evolution d'une cellule vivante
					if ( v!=2 && v!= 3 ) set_morte(i,j,*g);
					else if (vieillisement){
						if (g->cellules[i][j] >= 8)
							set_morte(i,j,*g);
						else
							g->cellules[i][j]++;
					}

				}
				else
				{ // evolution d'une cellule morte
					if ( v==3 ) set_vivante(i,j,*g);
				}
			}
		}
	}
	return;
}
