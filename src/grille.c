#include "grille.h"

extern int errno;

void setLineGrille(struct grille *g, u_int l);
void setCollumngGrille(struct grille *g, u_int c);
void createGrille( struct grille  * const g);


uint getGrilleCols(struct grille *grille);

uint getGrilleLines(struct grille *grille);

void init_grille_from_file (char * filename, grille* g){
	FILE * pfile = NULL;
	pfile = fopen(filename, "r");
	assert (pfile != NULL);

	int i,j,n,l,c,vivantes=0;

	fscanf(pfile, "%d", &l);
	fscanf(pfile, "%d", &c);
	printf("%c",' ');

	alloue_grille(l,c,g);

	fscanf(pfile, "%d", & vivantes);

	for (n=0; n < vivantes; ++n){
		fscanf(pfile, "%d", & i);
		fscanf(pfile, "%d", & j);
		set_vivante(i,j,*g);
	}
	int not_viable = 0;
	fscanf(pfile,"%d",&not_viable);
	for (int i = 0; i < not_viable; ++i) {
		fscanf(pfile,"%d",&l);
		fscanf(pfile,"%d",&c);
		set_not_viable(l,c,*g);
	}

	fclose (pfile);
}


void copie_grille (grille gs, grille gd){
	int i, j;
	for (i=0; i<gs.nbl; ++i) for (j=0; j<gs.nbc; ++j) gd.cellules[i][j] = gs.cellules[i][j];
	return;
}


void alloue_grille(int l, int c, grille* g){
	//todo test it
	//checkout for l & c beeing not <= 0 and g not null
	if (l < 0 && c < 0 && g == NULL)
	{
		int errnum = errno;
		fprintf(stderr, "Value of errno: %d\n", errno);
		perror("Error printed by perror");
		fprintf(stderr, "Error opening file: %s\n", strerror( errnum));
		exit(errnum);
	}
	// if its ok need to modify grille * g and create that shi*y grille
	setLineGrille(g,l);
	setCollumngGrille(g,c);
	createGrille(g);

}

void createGrille(struct grille *const g) {
	{
//		printf("%d",getGrilleLines(g));
		g->cellules = malloc(getGrilleLines(g) * sizeof(int*));
		if(!g->cellules)
		{
			fprintf(stderr, "Value of errno: %d\n", errno);
			perror("Error in g->cell creation");
			exit(errno);
		}

		for (int i = 0,
				     nblines = getGrilleLines(g),
				     nbccols = getGrilleCols(g);
		     i < nblines;++i)
		{

			if (!((g->cellules[i]) = calloc(nbccols , sizeof(int))))
			{
				//free used memm in case if opp system sucs
				for (int j = i-1; j >= 0 ; --j) {
					free(g->cellules[j]);
				}
				free(g->cellules);
				//left message
				fprintf(stderr, "Value of errno: %d\n", errno);
				perror("Error in g->cell creation");
				exit(errno);
			}
		}
	}


}


void libere_grille(struct grille * grille1){
	for (int line = 0, nlines = getGrilleLines(grille1); line < nlines; ++line) {
		free(grille1->cellules[line]);
	}
	free(grille1->cellules);

}




uint getGrilleLines(struct grille *grille) {
	return grille->nbl;
}

uint getGrilleCols(struct grille *grille) {
	return grille->nbc;
}

void setLineGrille(struct grille *g, u_int l) {
	g->nbl = l;
}

void setCollumngGrille(struct grille *g, u_int c) {
	g->nbc = c;
}

