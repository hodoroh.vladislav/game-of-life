
#include <stdlib.h>
#include <stdio.h>
#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>
#include <X11/Xlib.h>

#include "grille.h"
#include "io.h"
#include "jeu.h"

#define SIZEX 600
#define SIZEY 800

// test des colonies oscillantes
int cyclique=0;
uint vieillisement = 0;
int oscactive = 0, testgrille = 0, i, j, tmp=0, periode=0, vivante=0;

void paint(cairo_surface_t *surface, grille g)
{
	// create cairo mask
	cairo_t *cr;
	cr=cairo_create(surface);

	// background
	cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
	cairo_paint(cr);

	// text
	cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
	cairo_select_font_face(cr, "Ariel", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 40);

	cairo_move_to(cr, 200, 60);
	cairo_show_text(cr, "Jeu de la Vie");

	cairo_set_font_size(cr, 15);
	cairo_move_to(cr, 40, 750);
	if (cyclique == 1)
		cairo_show_text(cr, "Mode cyclique: activé");
	else
		cairo_show_text(cr, "Mode cyclique: désactivé");

	cairo_move_to(cr, 40, 775);
	if (oscactive == 1)
		cairo_show_text(cr, "Test colonie oscillante: activé");
	else
		cairo_show_text(cr, "Test colonie oscillante: désactivé");

	cairo_move_to(cr, 350, 775);
	if (testgrille == 2 && oscactive ==1)
		cairo_show_text(cr, "Colonie oscillante!");

	cairo_move_to(cr, 40, 725);
	if (vieillisement == 1)
		cairo_show_text(cr, "Vieillissement: activé");
	else
		cairo_show_text(cr, "Vieillissement: désactivé");

	// filled rectangle
	int pos_i, pos_j, colonne;

//	ligne = ((SIZEX)/g.nbl);

	colonne = (600-(5*(g.nbc+1)))/g.nbc;

	for (int i=0;i < g.nbl;i++) {
		for (int j=0;j<g.nbc;j++) {
			pos_i=100+i*(colonne+5);
			pos_j=5+j*(colonne+5);

			cairo_rectangle(cr,pos_j,pos_i,colonne,colonne);

			if (g.cellules[i][j]==0)
				cairo_set_source_rgb (cr, 0, 0, 0);

			else if (g.cellules[i][j]>1)
				cairo_set_source_rgb(cr, 0.8, 0.8, 0.8);


			else if (g.cellules[i][j]==-1)
				cairo_set_source_rgb (cr, 101.0, 101.0, 101.0);

			else
				cairo_set_source_rgb (cr, 0.5, 0.5, 0.5);

			cairo_fill(cr);
		}
	}
	// destroy cairo mask
	cairo_destroy(cr);
}

int main (int argc, char *argv[]) {
	if (argc != 2) {
		printf("Usage : %s <fichier grille>\n", argv[0]);
		return 1;
	}

	// init grilles
	grille g;
	grille gc;
	grille gcTest;
	init_grille_from_file(argv[1],&g);
	alloue_grille (g.nbl, g.nbc, &gc);
	compte_voisins_vivants = compte_voisins_vivants_nc;

	// X11 display
	Display *dpy;
	Window rootwin;
	Window win;
	XEvent e;
	int scr;

	// init the display
	if (!(dpy=XOpenDisplay(NULL))) {
		fprintf(stderr, "ERROR: Could not open display\n");
		exit(1);
	}

	scr=DefaultScreen(dpy);
	rootwin=RootWindow(dpy, scr);

	win=XCreateSimpleWindow(dpy, rootwin, 1, 1, SIZEX, SIZEY, 0,
	                        BlackPixel(dpy, scr), BlackPixel(dpy, scr));

	XStoreName(dpy, win, "Jeu de la Vie");
	XSelectInput(dpy, win, ExposureMask|ButtonPressMask|KeyPressMask);
	XMapWindow(dpy, win);

	// create cairo surface
	cairo_surface_t *cs;
	cs=cairo_xlib_surface_create(dpy, win, DefaultVisual(dpy, 0), SIZEX, SIZEY);

	// run the event loop
	while(True) {
		XNextEvent(dpy, &e);
		if (e.type==Expose && e.xexpose.count<1) {
			paint(cs,g);
		}
		else if (e.type==ButtonPress) {
			if (e.xbutton.button==1) {
				evolue(&g,&gc);
				paint(cs,g);
			}
			if (e.xbutton.button==3) {
				break;
			}
		}

		// test des colonies oscillantes
		if (e.type==KeyPress) {
			if (e.xkey.keycode==XKeysymToKeycode(dpy,'o')) {
				if (oscactive == 0) oscactive = 1;
				else {
					oscactive = 0;
					testgrille = 0;
					periode = 0;
				}
			}
		}

		if (oscactive == 1) {
			if (testgrille == 0) {
				// enregistrement etat initial grille
				alloue_grille (gc.nbl, gc.nbc, &gcTest);
				copie_grille(gc, gcTest);
				testgrille = 1;
			}
			else {
				if (testgrille == 1 && periode < 1000) {
					vivante = 0;
					for (i=0; i<gc.nbl; i++) {
						for (j=0; j<gc.nbc; j++) {
							if (gc.cellules[i][j] != gcTest.cellules[i][j]) tmp++;
							if (est_vivante(i, j, gc) == True) vivante++;
						}
					}
					if (tmp == 0) testgrille = 2;
					if (vivante == 0) {
						oscactive = 0;
						testgrille = 0;
						periode = 0;
					}
					tmp = 0;
					periode++;
				}
				else {
					if (testgrille != 2)
					{
						oscactive = 0;
						testgrille = 0;
						periode = 0;
					}
				}
			}
		}

		if (e.type==KeyPress) {
			if (e.xkey.keycode==XKeysymToKeycode(dpy,'n')) {
				// touche "n" pour ouvrir une nouvelle grille
				libere_grille(&g);
				libere_grille(&gc);
				printf("Entrez le nom de la nouvelle grille à charger: ");
				char nouv_grille[128];
				if(!scanf("%s", nouv_grille))
					perror("Cairo scanf error");
				init_grille_from_file(nouv_grille, &g);
				alloue_grille (g.nbl, g.nbc, &gc);
				testgrille = 0;
				oscactive = 0;
				periode = 0;
				paint(cs,g);
			}

			if (e.xkey.keycode==XKeysymToKeycode(dpy,'c')) {
				// touche "c" pour activer/désactiver le voisinage cyclique
				if ( cyclique == 1) {
					compte_voisins_vivants = compte_voisins_vivants_nc;
					cyclique = 0;
				}
				else{
					compte_voisins_vivants = compte_voisins_vivants_c;
					cyclique = 1;
				}
			}

			if (e.xkey.keycode==XKeysymToKeycode(dpy,'v')) {
				// touche "v" pour activer ou désactiver le vieillissement
				if (vieillisement == 0) {
					vieillisement = 1;
				}
				else{
					vieillisement = 0;
				}
			}


		}
	}

	cairo_surface_destroy(cs); // destroy cairo surface
	XCloseDisplay(dpy); // close the display
	return 0;
}

