.PHONY: clean bin/gol doc

vpath %.c src
vpath %.h include
OPATH=obj

CFLAGS += -Og -g -Wall -Wextra
CPPFLAGS += -Iinclude -I/usr/include/cairo
LDFLAGS += -lcairo -lm -lX11
DOXYGEN = doxygen

ifeq (TEXTE,$(MODE))
all:mkdirs
mkdirs:
	mkdir -p  obj lib bin
	make bin/gol

bin/gol : obj/main.o lib/libjeu.a
	$(CC) $(CFLAGS) -o $@ $^ -L -ljeu

$(OPATH)/main.o : main.c io.h grille.h jeu.h
$(OPATH)/io.o : io.c io.h grille.h jeu.h
$(OPATH)/jeu.o : jeu.c jeu.h grille.h
$(OPATH)/grille.o : grille.c grille.h

$(OPATH)/%.o : %.c | $(OPATH)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

lib/libjeu.a : $(OPATH)/main.o $(OPATH)/io.o $(OPATH)/jeu.o $(OPATH)/grille.o
	ar rcs lib/libjeu.a $(OPATH)/main.o $(OPATH)/io.o $(OPATH)/jeu.o $(OPATH)/grille.o

else
all:mkdirs
mkdirs:
	mkdir -p  obj lib bin
	make bin/gol
bin/gol : obj/cairo.o lib/libjeu.a
	$(CC) $(CFLAGS) -o $@ $^ -L -ljeu $(LDFLAGS)

$(OPATH)/cairo.o : cairo.c io.h grille.h jeu.h
$(OPATH)/io.o : io.c io.h grille.h jeu.h
$(OPATH)/jeu.o : jeu.c jeu.h grille.h
$(OPATH)/grille.o : grille.c grille.h

$(OPATH)/%.o : %.c | $(OPATH)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

lib/libjeu.a : $(OPATH)/cairo.o $(OPATH)/io.o $(OPATH)/jeu.o $(OPATH)/grille.o
	ar rcs lib/libjeu.a $(OPATH)/cairo.o $(OPATH)/io.o $(OPATH)/jeu.o $(OPATH)/grille.o

endif

dist :
	make clean
	tar --exclude='./.*' --exclude='*.gz' --exclude='./cmake-build-debug' --exclude='./archs' -zcvf "HODOROH-Vladislav-GoL_$(shell git describe)_.tar.gz" .
#	$(shell ls | egrep -v "[^src$,^include$,^*.c$,^Doxyfile$,^Makefile$,^CMakeList$,.^gitignore$]")


doc :
	$(DOXYGEN) Doxyfile

clean :
	-rm -rf *.o doc $(OPATH)/main.o $(OPATH)/cairo.o $(OPATH)/grille.o $(OPATH)/io.o $(OPATH)/game.o bin/gol lib/libgame.a