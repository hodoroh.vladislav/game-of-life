/**
 * @file grille.h
 */
#ifndef __GRILLE_H
#define __GRILLE_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "errno.h"
#include <string.h>

// structure grille : nombre de lignes, nombre de colonnes, tableau de tableau de cellules
/**
 * @typedef grille typedef du grille
 * @struct grille
 * @brief declaration du type grille du structure grille
 */
typedef struct grille {int nbl; int nbc; int** cellules;} grille;
 /**
  * @brief alloue une grille de taille l*c, et initialise toutes les cellules à mortes
  * @fn
  * @param l
  * @param c
  * @param g
  */

void alloue_grille (int l, int c, grille* g);

/**
 * @brief libère une grille
 * @param g
 */
void libere_grille (grille* g);

/**
 * @brief alloue et initalise la grille g à partir d'un fichier
 * @param filename
 * @param g
 */
void init_grille_from_file (char * filename, grille* g);

/**
 * \brief rend vivante la cellule (i,j) de la grille g
 * @param i
 * @param j
 * @param g
 */
static inline void set_vivante(int i, int j, grille g){g.cellules[i][j] = 1;}
/**
 * @brief rend morte la cellule (i,j) de la grille g
 * @param i
 * @param j
 * @param g
 */
static inline void set_morte(int i, int j, grille g){g.cellules[i][j] = 0;}
/**
 * @brief teste si la cellule (i,j) de la grille g est vivante
 * @param i
 * @param j
 * @param g
 * @return
 */
static inline int est_vivante(int i, int j, grille g){return g.cellules[i][j] >= 1;}




/**
 * \brief copie la grille
 * @param gs
 * @param gd
 */
void copie_grille (grille gs, grille gd);

/**
 * @brief indique si la cellule est viable
 * @param i
 * @param j
 * @param g
 * @return
 */
inline int est_viable(int i,int j,grille g){
	return (g.cellules[i][j] != -1);
}

// vFractal functions declaration
static inline void set_not_viable(int i, int j, grille g){g.cellules[i][j] = (-1);}



#endif
