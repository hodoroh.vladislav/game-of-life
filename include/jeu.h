/**
 * @file jeu.h
 */
#ifndef __JEU_H
#define __JEU_H

#include "grille.h"

typedef enum{
	false,true
} bool;

// modulo modifié pour traiter correctement les bords i=0 et j=0
// dans le calcul des voisins avec bords cycliques
/**
 * @fn static inline int modulo(int i, int m)
 * @brief (i + m) % m
 * @param i
 * @param m
 * @return
 */
static inline int modulo(int i, int m) {return (i+m)%m;}

// compte le nombre de voisins vivants de la cellule (i,j)
// les bords sont cycliques.

/**
 * @fn int compte_voisins_vivants(int i, int j, grille g)
 * @biref compte vivants cyclique
 * @param i index i
 * @param j index j
 * @return voisins vivants
 */
int compte_voisins_vivants_c (int i, int j, grille g);

/**
 * @fn compte_voisins_vivants_nc(int,int,grille);
 * @biref compte vivants non cyclique
 * @param i index i
 * @param j index j
 * @return
 */
int compte_voisins_vivants_nc(int,int,grille);


// fait évoluer la grille g d'un pas de temps
/**
 * @fn void evolue(grille*g,grille*gc)
 * @param *g pointeur vers grille
 * @param *gc pointeur vers copie grille
 * @return void
 */
void evolue (grille *g, grille *gc);

/**
 * @fn void incrVielliesement(grille)
 * @brief incrementer le viellisement
 */
void incrVielliesement(grille);


/**
 * @brief Pointeur de fonction pour changer dynamiquement la mettode du comptage des voisins
 * @return
 */
int (*compte_voisins_vivants)(int, int,grille);


#endif
