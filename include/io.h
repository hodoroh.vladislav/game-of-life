/**
 * @file io.h
 */
#ifndef __IO_H
#define __IO_H

#include <stdio.h>
#include "grille.h"
#include "jeu.h"

/**
 * @fn void affiche_trait(int c)
 * @param int c nombre des traits a afficher
 * @param c
 *
 */
void affiche_trait (int c);

/**
 * @fn void affihce_ligne(int c, int* ligne)
 * @param int c nombre des colones
 * @param int* ligne pointeur vers une ligne du grille
 * @param c
 * @param ligne
 */
void affiche_ligne (int c, int* ligne);


/**
 * @fn void affiche_grille(grille g)
 * @param grille g grille
 * @brief affiche la grille
 * @param g
 */
void affiche_grille (grille g);

/**
 * @fn clear grille on console
 * @param g
 */
void efface_grille (grille g);

/**
 * @fn void debut_jeu(grille *g, grille *gc)
 * @param g grille
 * @param gc copie du grille g
 * @brief debut du jeu
 * @param g
 * @param gc
 */
void debut_jeu(grille *g, grille *gc);

// vFractal functions declaration
/**
 * @fn print evolution
 * @param evolution
 */
void afficje_evolution(uint evolution);



// vfractal function declarations
/**
 * @fn void affiche_line_viellisement(int c, int * ligne)
 * @brief affiche la ligne mais la cellule porte sa generation
 * @param c
 * @param ligne
 */
void affiche_line_vielliesement(int c, int * ligne);




#endif
